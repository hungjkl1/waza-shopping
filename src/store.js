import { createStore, compose,applyMiddleware } from "redux";
import thunk from 'redux-thunk';
import root from "./reducer";
const initialState = {};
const middleware = [thunk]
let store = null
if(process.env.REACT_APP_ENV==='DEV'){
    store = createStore(
      root,
      initialState,
      compose(
        applyMiddleware(...middleware),
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
      )
    );
} else {
  store = createStore(
    root,
    initialState,
    compose(
      applyMiddleware(...middleware),
    )
  );
}
export default store;
