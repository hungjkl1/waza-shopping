import Parse from 'parse';
import Swal from 'sweetalert2';
import { openRatingForm } from '../containers/Home/Rating/actions';
import store from '../store';

export const liveQuery = async (user) => {
    Parse.initialize('hHdDuClDuzXuRxnYmWOzNZ4qxZW7MVHq61u','GtE9bEirZcp2eH4y95qb4me668oP8TM9UhA')
    Parse.serverURL=process.env.REACT_APP_SERVER_URL
    Parse.liveQueryServerURL=process.env.REACT_APP_SERVER_LIVE_QUERY
    console.log(Parse.liveQueryServerURL)
    let query = new Parse.Query('Bill');
    query.equalTo('user.objectId',user.objectId)
    query.include('shop')
    let subscription = await query.subscribe();
    subscription.on('open', () => {
      console.log('subscription opened');
     });
     subscription.on('update', bill => {
         console.log('here')
        const status = bill.get('State')
        console.log(status)
        switch (status) {
            case 'ORDER CANCELED':
            {
                return Swal.fire({
                title: `Order Number: ${bill.id} have been canceled`,
                text: 'Your order have canceled because the driver cannot take this trip',
                })
                
            }
            case 'ORDER CONFIRM FINISH': {
                store.dispatch(openRatingForm(bill.toJSON()))
                break;
            }
            case 'ORDER HAVE DRIVER': {
                const myBill = bill.toJSON()
                const {driver = {}} = myBill
                console.log(myBill)
                Swal.fire(`Order No.${myBill.objectId} have driver`,`Driver:${driver.fullName}, Phone:${driver.phone} have take your order`)
                break;
            
            }
            case 'ORDER CANCEL BY DRIVER': {
                const myBill = bill.toJSON()
                const {driver = {}} = myBill
                console.log(myBill)
                Swal.fire(`Order No.${myBill.objectId} have been canceled by driver`,`Driver:${driver.fullName}, Phone:${driver.phone} have canceled your order`)
                break;
            
            }
            case 'ORDER CANT FIND DRIVER': {
                const myBill = bill.toJSON()
                console.log(myBill)
                Swal.fire(`Order No.${myBill.objectId} can't find any driver`)
                break;
            }
            default:
                break;
        }
       });
  }