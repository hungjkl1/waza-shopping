import { combineReducers } from "redux";
import { reducer as formReducer } from 'redux-form'
import authUser from '../containers/AuthUser/reducer';
import shopList from '../containers/Home//MainHome/reducer';
import shopDetail from '../containers/ShopDetail/reducer'
import userAddress from '../components/Profile/UserAddress/reducer'
import cartItems from '../containers/ShoppingCart/reducer'
import bills from '../components/Profile/CustomerWallet/reducer'
import ratingModal from '../containers/Home/Rating/reducer'
const reducer = combineReducers({
    form: formReducer,
    authUser,
    shopList,
    shopDetail,
    userAddress,
    cartItems,
    bills,
    ratingModal
});
export default reducer;