import React, { Component } from "react";
import "./style.scss";
// Redux
import { connect } from "react-redux";
import { addProduct } from "../../containers/ShoppingCart/action";

class Item extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productQuanity: 1
    };
  }
  handleOnChange = e => {
    if (parseInt(e.target.value) > 0) {
      this.setState({
        productQuanity:
          parseInt(e.target.value) >= 100 ? 99 : parseInt(e.target.value)
      });
    }
  };

  addProductToCart = () => {
    const item = {
      ...this.props.item,
      quantity: this.state.productQuanity
    };
    this.props.dispatch(addProduct(item));
  };

  render() {
    return (
      <div className="panel-body">
        <div>
          {/* LIST */}
          <div id="shop-menus-item" data-section="menu">
            <div className="row ">
              <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div className="shop-food-menu ">
                  {/* ITEM */}
                  <ul>
                    <li>
                      <div className="shop-food-desc text-left">
                        <div className="product-img">
                          <img
                            src="image/product-holder.jpg"
                            className="img-fluid"
                            alt="itemimg"
                          />
                        </div>
                        <div className="product-info">
                          <h6>{this.props.item.name}</h6>
                          <div className="product-info-desc">
                            {this.props.item.description}
                          </div>
                        </div>
                      </div>

                      <div className="shop-food-pricing">
                        <div className="price">
                          {this.props.item.price} VND/{this.props.item.Unit}
                        </div>
                        <div className="quantity">
                          <input
                            type="number"
                            value={this.state.productQuanity}
                            onChange={this.handleOnChange}
                            name="quantity"
                            min="1"
                          />
                        </div>
                        {/* Nút thêm sản phẩm vào giỏ hàng */}
                      </div>
                    </li>
                  </ul>
                  {/* // */}
                </div>
              </div>
            </div>
          </div>
          {/* // */}
        </div>
        <div className="btn-add-container">
          <button
            className="btn btn-block btn-info btn-addtocart"
            onClick={() => this.addProductToCart(this.props.item)}
          >
            Add to cart
          </button>
        </div>
      </div>
    );
  }
}
export default connect(null, null)(Item);
