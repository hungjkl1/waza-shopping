import React, { Component } from 'react';
import { Card } from 'react-bootstrap';
import './style.scss';
import Item from './Item';

class ShopItem extends Component {
    render() {
        console.log(this.props.product)

        let elements = this.props.product.map((item, index) => {
            return <Item addProduct={this.props.addProduct} item={item} key={item.objectId}/>
        });
        
        return (
            <div style={{ marginBlockEnd: "10%" }}>
                <Card className="panel panel-primary">
                    {/* KHUNG SEARCH + CART */}
                    <Card.Header className="panel-heading">
                        <div className="row">
                            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <div className="input-group">
                                    <input type="text"
                                        className="form-control"
                                        placeholder="Type here to search..." />
                                    &nbsp;
                                    <button className="btn btn-sm btn-info" type="button">
                                        <span className="fa fa-search" id="search_icon"> 
                                        &nbsp;Search</span>
                                    </button>
                                </div>
                            </div> 
                        </div>
                    </Card.Header>
                    <Card.Body>
                        {elements}
                    </Card.Body>
                </Card>
            </div>
        );
    }
}

export default ShopItem;