import React, { Component } from 'react';
import ShopCell from './ShopCell';
import './style.scss'


export class ShopList extends Component {
    render() {
        return (
            <div className='container-fluid'>
                <div className='row shop-list'>
                    {this.props.list.map(item =>
                        <div className='col-12 shop-list-cell' key={item.objectId}>
                            <ShopCell item={item} />
                        </div>
                    )}
                </div>
            </div>
        );
    }
}

export default ShopList;
