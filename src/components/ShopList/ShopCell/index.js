import React, { Component } from "react";
import {Link} from '@reach/router'
import './style.scss'

export class ShopCell extends Component {
  render() {
    return (
      <Link className='link-shop-cell' to={`shop/${this.props.item.objectId}`}>
      <div className="shop-cell">
        <div className='shop-cell-image'>
          <img src="image/shop-icon.png" alt='img'/>
        </div>
        <div className='shop-cell-info'>
            <div className='shop-cell-info-title'>
                {this.props.item.name}
            </div>
            <div className='shop-cell-info-content'>
            <div className='shop-cell-info-content-address'>
            <i className="fa fa-map-marker"/>
            {' '}
              {this.props.item.address}
            </div>
            <div className='shop-cell-info-content-phone'>
            <i className="fa fa-phone"></i>
            {' '}
            {this.props.item.phone}
          </div>
          <div className='shop-cell-info-content-time'>
          <i className="fa fa-clock-o"></i>
          {' '}
          {/* {this.props.item.openTime}~{this.props.item.closeTime} */}
        </div>
            </div>
        </div>
      </div>
      </Link>
    );
  }
}

export default ShopCell;
