import React, { Component } from "react";
import Select,{components} from "react-select";
import { connect } from "react-redux";
import "./style.scss";
import axios from "axios";

const Option = ({ children, ...props }) => {
  return (
      <components.Option {...props}>
          {props.data.Code}
      </components.Option>
  );
};
const SingleValue = ({ children, ...props }) => {
  return (
      <components.SingleValue {...props}>
        {props.data.Code}
      </components.SingleValue>
  );
};

class VoucherReward extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list: []
    };
  }

  componentDidMount() {
    axios
      .post(
        "https://safe-caverns-44957.herokuapp.com/API/Reward/GetListReward",
        {
          uid: this.props.user.coreId,
          serviceID: "5e02b6fc66c3fb0024f435bf"
        }
      )
      .then(result => {
        this.setState({
          list: result.data.value
        });
      });
  }
  render() {
    console.log(this.state.list);
    return (
      <div>
        <Select
          onChange={value => {
            this.props.onChangeCombo(value);
          }}
          isDisabled={this.props.isDisable()}
          placeholder="reward-voucher"
          classNamePrefix="reward-select"
          isClearable
          options={this.state.list}
          isSearchable={false}
          components={{
            Option:Option,
            SingleValue:SingleValue
          }}
        />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  user: state.authUser.user
});

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(VoucherReward);
