import React, { Component } from 'react'
import Select,{components} from 'react-select'
import {connect} from 'react-redux'
import './style.scss'
const Option = ({ children, ...props }) => {
    return (
        <components.Option {...props}>
          {props.data.address}
        </components.Option>
    );
  };
const SingleValue = ({ children, ...props }) => {
    return (
        <components.SingleValue {...props}>
          {props.data.address}
        </components.SingleValue>
    );
  };
class LocationSelect extends Component {

    render() {
        return (
            <div>
            <Select
            onChange={value=>{
                this.props.onChangeLocation(value)
            }}
            getOptionValue={option => option}
            placeholder='Your Address...'
            classNamePrefix='location-select' 
            options={this.props.location}
            isSearchable={false}
            components={
                {
                    Option:Option,
                    SingleValue:SingleValue
                }
            }
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    location: state.userAddress.location
})

const mapDispatchToProps = {
    
}


export default connect(mapStateToProps, mapDispatchToProps)(LocationSelect)