import React, { Component } from "react";
import "./style.scss";
import { Form, Row, Col } from "react-bootstrap";

class Info extends Component {
  render() {
    return (
      <Form className="form-input">
        <Form.Group as={Row} controlId="formPlaintextLastname">
          <Form.Label column sm="2">
            Full Name
          </Form.Label>
          <Col sm="4">
            <Form.Control type="text" placeholder="Lastname" readOnly value={this.props.user.fullName} />
          </Col>
        </Form.Group>

        {/*                <Form.Group as={Row} controlId="formPlaintextGender">
                    <Form.Label column sm="2">
                        Gender
                                </Form.Label>
                    <Col sm="4">
                        <Form.Control as="select">
                            <option>Female</option>
                            <option>Male</option>
                        </Form.Control>
                    </Col>
                </Form.Group>
        */}
        <Form.Group as={Row} controlId="formPlaintextEmail">
          <Form.Label column sm="2">
            email
          </Form.Label>
          <Col sm="4">
            <Form.Control plaintext readOnly defaultValue={this.props.user.email} />
          </Col>
        </Form.Group>

        <Form.Group as={Row} controlId="formPlaintextPassword">
          <Form.Label column sm="2">
            Password
          </Form.Label>
          <Col sm="4">
            <span className="show-pass">***************</span>
          </Col>
          <Col sm="3">
            <button className="change-pass">Change password</button>
          </Col>
        </Form.Group>
      </Form>
    );
  }
}

export default Info;
