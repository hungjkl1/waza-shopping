import React, { Component } from 'react';
import './style.scss';

class Phone extends Component {
    render() {
        return (
            <div className="list-phone">
                <div className="list-phone-row">
                    <div className="row align-items-center">
                        <div className="col-1 txt-bold">1.</div>
                        <div className="col-3 txt-bold">0942540906</div>
                        <div className="col-3">
                            <input type="radio" name="verify-phone" checked />
                            &nbsp;
                            <label>Number primary</label>
                        </div>
                        <div className="col-3">
                            <span className="phone-verified">
                                <i className="fa fa-check-circle txt-green" />
                                &nbsp;
                                Phone verified
                            </span>
                        </div>
                        <div className="col-2">
                            <button className="btn-edit" type="button">EDIT</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Phone;