import React, { Component } from 'react';
import './style.scss';
import Avatar from './Avatar';
import Info from './Info';
// import Phone from './Phone';

class CustomerInfo extends Component {
    render() {
        return (
            <div className="customer-info-main">
                <div className="main-info-header">
                    User information
                </div>
                <div className="main-info-content">

                    <div className="profile-picture-update">
                        <div className="title-user">Update profile picture</div>
                        <Avatar />
                    </div>
                    <hr />

                    <div className="user-info-update">
                        <div className="title-user">Change info</div>
                        <Info user={this.props.user}/>
                        <div className="col-3">
                            <button className="btn-save" type="submit">Save change</button>
                        </div>
                    </div>
                    <hr/>

                </div>
            </div>
        );
    }
}

export default CustomerInfo;