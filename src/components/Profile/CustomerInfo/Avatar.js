import React, { Component } from 'react';
import './style.scss';
import { Image } from 'react-bootstrap';

class Avatar extends Component {
    render() {
        return (
            <div className="row">
                <div className="col-12">
                    <Image id="avatar_user" src="https://www.colibrilife.com/wp-content/uploads/2018/01/no-avatar-female.png" roundedCircle />
                </div>
                {/*<div className="col-10">
                    <div className="upload-image">
                        <span>Upload from</span>
                        &nbsp;
                        <label className="label-custom" htmlFor="validateCustomFile">Choose </label>
                        &nbsp;
                        <span style={{ fontSize: "12px", fontStyle: "Italic" }}>GIF, JPEG, PNG, BMP accepted with a maximum size of 5.0 MB</span>
                    </div>
                    <button className="btn-update">Update</button>
            </div>*/}
            </div>
        );
    }
}

export default Avatar;