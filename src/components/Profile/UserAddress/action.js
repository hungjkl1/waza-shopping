export const FETCH_USER_ADDRESS = 'FETCH_USER_ADDRESS'

export const fetchUserAddress = (payload) => ({
    type: FETCH_USER_ADDRESS,
    payload
})
