import React, { Component } from "react";
import LocationModal from "../../../containers/LocationModal";
import { Card } from "react-bootstrap";
import { addAddress, fetchUserLocation, deleteAddress } from "./thunk";
import { connect } from "react-redux";
import "./style.scss";
import moment from "moment";
class UserAddress extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLocationModal: false
    };
  }

  handleOpenClose = () => {
    this.props.fetchUserLocation();
    this.setState({
      showLocationModal: !this.state.showLocationModal
    });
  };
  handleAddAddress = value => {
    addAddress(value);
  };
  render() {
    return (
      <div>
        <Card>
          <Card.Header>Address</Card.Header>
          <Card.Body>
            <div className="user-address">
              {this.props.address.map(item => {
                return (
                  <div className="user-address-cell" key={item.objectId}>
                    <div className="user-address-cell-title">
                      {item.address}
                    </div>
                    <button
                      onClick={() => {
                        this.props.deleteAddress({ shopId: item.objectId });
                      }}
                      className="btn btn-danger"
                    >
                      X
                    </button>
                  </div>
                );
              })}
            </div>
            <button
              onClick={this.handleOpenClose}
              className="btn btn-info btn-block"
            >
              Add Address
            </button>
          </Card.Body>
        </Card>
        <Card className="bill-container-header">
          <Card.Header>Your Bill</Card.Header>
          <Card.Body className="bill-container">
            {this.props.bills.map(item => {
              return (
                <div className="bill-container-item" key={item.objectId}>
                  <div className="row">
                    <div className="col-12 bill-container-item-header row">
                      <div className="col-12  text-center"  >
                        {item.State === 'ORDER CONFIRM FINISH' ?
                          (<div className="state-bill-DONE">
                            {item.State}
                          </div>) : 
                            (<div className="state-bill-else">
                              {item.State}
                            </div>)              
                        }
                      </div>
                      <div className="col-12 ">
                        <div>
                          <b>Bill number :</b> {item.objectId}
                        </div>
                        <div>
                          <b>Order Date :</b>{" "}
                          {moment(item.createdAt)
                            .format("DD/MM/YYYY HH:mm")
                            .toString()}
                        </div>
                      </div>

                    </div>
                    <div className="col-12 bill-container-item-body">
                      <div className="row">
                        <div className="col-12">
                          <b> Name Shop :</b> {item.shop.name}
                        </div>
                        <div className="col-12">
                          <b> Phone :</b> {item.shop.phone}
                        </div>
                        <div className="col-12">
                          <b> Shop Address :</b> {item.start_address}
                        </div>
                        <div className="col-12">
                          <b> Your Address :</b> {item.end_address}
                        </div>
                        <hr />
                        <div className="col-12">
                          <b> Pay Options : </b> {item.payWith}
                        </div>
                        <div className="row col-12">

                          <div className="col-xl-6 col-lg-6 col-md-6 ">
                            {item.discount &&
                              (<div className="col-12 ">
                                <b> Dicount : </b> {item.discount}
                              </div>)
                            }
                            {item.reward &&
                              (<div className="col-12 ">
                                <b> Reward : </b> {item.reward}
                              </div>)
                            }
                          </div>
                          <div  className="col-12 col-xl-6 col-lg-6 col-md-6 total">
                            <div className="col-12 total">
                              <b> Sub Total : </b> {item.subtotal}
                            </div>
                            <div className="col-12 total">
                              <b> Shipping Fee : </b> {item.shippingFee}
                            </div>
                           
                            <div className="col-12 total">
                              <b> TOTAL : </b> {item.total}
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              );
            })}
          </Card.Body>
        </Card>
        <LocationModal
          handleAddAddress={this.handleAddAddress}
          show={this.state.showLocationModal}
          handleOpenClose={this.handleOpenClose}
        />
      </div>
    );
  }
}
const mapStateToProps = state => ({
  address: state.userAddress.location,
  bills: state.bills.list
});

const mapDispatchToProps = dispatch => {
  return {
    deleteAddress: params => dispatch(deleteAddress(params)),
    fetchUserLocation: () => dispatch(fetchUserLocation())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(UserAddress);
