import { FETCH_USER_ADDRESS } from "./action"

const initialState = {
    location:[]
}

export default (state = initialState, { type, payload }) => {
    switch (type) {

    case FETCH_USER_ADDRESS:
    const address = {
        location:payload.data
    }    
    return address

    default:
        return state
    }
}
