import API from '../../../core/service'
import Swal from 'sweetalert2'
import { fetchUserAddress } from './action';
const service = new API();
export const addAddress = (params) => {
    service.post('createLocation',params).then((result) => {
        return Swal.fire({
            text: 'Add address success',    
            confirmButtonText: 'Done'
        })
    }).catch((err) => {
        return Swal.fire({
            text: 'Add address failed',
            type: 'error',
            confirmButtonText: 'Please add again'
        })
    });
}

export const deleteAddress = (params) => {
    return dispatch => {
        service.post('deleteLocation',params).then(result=>{
          dispatch(fetchUserLocation())
        }).catch(err=>{
            return Swal.fire({
                text: 'Remove address failed',
                type: 'error',
                confirmButtonText: 'Please remove again'
            })
        })
    }
}

export const fetchUserLocation = () => {
    return dispatch => {
        service.post('getLocation').then((result)=>{
            dispatch(fetchUserAddress(result))
        }).catch(err => {
            console.log(err)
        } )
    }
}