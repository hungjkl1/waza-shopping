import { GET_CUSTOMER_BILLS } from "./action"

const initialState = {
    list: [],
    count: null
}
const bills = (state = initialState, { type, payload }) => {
    switch (type) {
    case GET_CUSTOMER_BILLS:{
        console.log(payload)
        const bills = {
            list: payload.data.results,
            count: payload.data.count
        }
    return bills}
    default:
        return state
    }
}
export default bills