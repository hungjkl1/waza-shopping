export const GET_CUSTOMER_BILLS = 'GET_CUSTOMER_BILLS'
export const getCustomerBills = (payload) => ({
    type: GET_CUSTOMER_BILLS,
    payload
})
