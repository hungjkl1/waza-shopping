import React, { Component } from 'react';
import CustomerAddWallet from './form';
import { connect } from 'react-redux'
import { addWalletThunk } from '../../../containers/AuthUser/thunk';
import { getBills, getMoney, addCash, transferWallet, transferCash } from './thunk'

export class CustomerWallet extends Component {
  constructor(props) {
    super(props)
    this.state = {
      wallet: 0,
      cash: 0
    }
  }
  onSubmitCash = async (value) => {
    await addCash(this.props.user.coreId, value)
    this.props.getBills()
    this.props.getWalletInfo(this.props.user.coreId)
  }

  onSubmitWallet = async (value) => {
    await transferWallet(this.props.user.coreId, value, this.props.balance.cash)
    this.props.getBills()
    this.props.getWalletInfo(this.props.user.coreId)
  }
  onSubmitWalletToCash = async (value) =>{
    await transferCash(this.props.user.coreId, value, this.props.balance.wallet)
    this.props.getBills()
    this.props.getWalletInfo(this.props.user.coreId)
  }
  componentDidMount() {
    this.props.getBills()
    this.props.getWalletInfo(this.props.user.coreId)
  }
  render() {
    return (
      <div>
        <div>
          <b>Your cash:{' '}</b>
          {this.props.balance.cash} VND
          <br />
          <b>Your wallet:{' '}</b>
          {this.props.balance.wallet} VND
        </div>
        <CustomerAddWallet addTo='Cash' onSubmit={this.onSubmitCash} />
        <hr />
        <CustomerAddWallet from='cash' addTo='Wallet' onSubmit={this.onSubmitWallet} />
        <hr />
        <CustomerAddWallet from='wallet' addTo='Cash' onSubmit={this.onSubmitWalletToCash} />
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  user: state.authUser.user,
  balance: state.authUser.balance ? state.authUser.balance : {}
})

const mapDispatchToProps = dispatch => ({
  addWallet: params => dispatch(addWalletThunk(params)),
  getWalletInfo: id => dispatch(getMoney(id)),
  getBills: () => dispatch(getBills())
})


export default connect(mapStateToProps, mapDispatchToProps)(CustomerWallet)
