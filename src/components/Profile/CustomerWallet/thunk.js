import API from "../../../core/service";
import { getCustomerBills } from "./action";
import axios from "axios";
import Swal from "sweetalert2";
import { getWalletInfo } from "../../../containers/AuthUser/action";
const service = new API();

export const getBills = () => {
    return dispatch => service.post('getUserBills').then((result) => {
        dispatch(getCustomerBills(result))
    }).catch((err) => {
        console.log(err)
    });
}

export const getMoney = id => {
    return async dispatch => {
        try {
            const wallet = await axios.get(`${process.env.REACT_APP_SERVER_PAYMENT}/wallet/${id}`)
            const cash = await axios.get(`${process.env.REACT_APP_SERVER_PAYMENT}/cash/${id}`)
            return dispatch(getWalletInfo({
                wallet: wallet.data.value,
                cash: cash.data.value
            }))
        } catch (err) {
            Swal.fire('Cant get payment detail', 'Server cant get any informations of you', 'error')
            return dispatch(getWalletInfo({
                wallet: 0,
                cash: 0
            }))
        }
    }
}

export const addCash = async (id, value) => {
    try {
        const cash = await axios.post(`${process.env.REACT_APP_SERVER_PAYMENT}/cash`, { user_id: id, value: Number(value.money) })
        if (cash.data.response === 'SUCCESS') {
            Swal.fire('Add money to cash success', `You just add ${value.money}VND to your cash wallet`, 'success')
        } else {
            Swal.fire('Cant add money to cash', 'Something wrong cant add to cash wallet', 'error')
        }
    } catch (err) {
        console.log(err)
        Swal.fire('Cant add money to cash', 'Something wrong cant add to cash', 'error')
    }
}

export const transferWallet = async (id, value, cash) => {
    try {
        if (Number(value.money) < cash) {
            const cash = await axios.post(`${process.env.REACT_APP_SERVER_PAYMENT}/transfer_from_cash`, { user_id: id, value: Number(value.money) })
            if (cash.data.response === 'SUCCESS') {
                Swal.fire('Transfer money to cash success', `You just transfer ${value.money}VND from cash to your waza wallet`, 'success')
            } else {
                Swal.fire('Cant transfer money to cash', 'Something wrong cant transfer to cash wallet', 'error')
            }
        }
        else {
            Swal.fire('Cant transfer cash to wallet', 'You are trying to transfer more cash than you have, please add to cash first', 'error')
        }
    } catch (err) {
        console.log(err)
        Swal.fire('Cant add money to cash', 'Something wrong cant add to cash', 'error')
    }
}

export const transferCash = async (id, value, wallet) => {
    try {
        if (Number(value.money) < wallet) {
            const wallet = await axios.post(`${process.env.REACT_APP_SERVER_PAYMENT}/transfer_from_wallet`, { user_id: id, value: Number(value.money) })
            if (wallet.data.response === 'SUCCESS') {
                Swal.fire('Transfer money to cash success', `You just transfer ${value.money}VND from cash to your waza wallet`, 'success')
            } else {
                Swal.fire('Cant transfer money to cash', 'Something wrong cant transfer to cash wallet', 'error')
            }
        }
        else {
            Swal.fire('Cant transfer wallet to cash', 'You are trying to transfer more wallet than you have, please add to wallet first', 'error')
        }
    } catch (err) {
        console.log(err)
        Swal.fire('Cant add wallet to cash', 'Something wrong cant add to cash', 'error')
    }
}