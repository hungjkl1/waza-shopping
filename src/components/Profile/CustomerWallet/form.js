import React, { Component } from 'react';
import InputComponent from '../../InputComponent';
import { Field, reduxForm } from "redux-form";

class CustomerAddWallet extends Component {
    render() {
        return (
            <form onSubmit={this.props.handleSubmit}>
                <Field type="number" name="money" placeholder="Input your money here...." min={50000} component={InputComponent}/>
                <button className='btn btn-primary btn-block mt-2' type='submit'> Add {this.props.from ? this.props.from : 'money'} to {this.props.addTo} </button>
            </form>
        );
    }
}

 export default reduxForm({
    form: "wallet"
  })(CustomerAddWallet);
