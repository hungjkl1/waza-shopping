import React, { Component } from 'react';
import './style.scss';
import { Tab, Row, Col } from 'react-bootstrap';
import CustomerInfo from './CustomerInfo';
import NavProfile from './NavProfile';
import {connect} from 'react-redux'
import UserAddress from './UserAddress';
import CustomerWallet from './CustomerWallet';


class Profile extends Component {
    render() {
        return (
            <div className="container">
                <Tab.Container id="list-group-tabs-example" defaultActiveKey="#link1">
                    <Row>
                        <Col sm={3}>
                            <NavProfile user={this.props.user}/>
                        </Col>
                        <Col sm={9}>
                            <Tab.Content>
                                <Tab.Pane eventKey="#link1">
                                   <CustomerInfo user={this.props.user} />
                                </Tab.Pane>
                                <Tab.Pane eventKey="#link2">
                                    <UserAddress/>
                                </Tab.Pane>
                                <Tab.Pane eventKey="#link3">
                                    <CustomerWallet/>
                                </Tab.Pane>
                            </Tab.Content>
                        </Col>
                    </Row>
                </Tab.Container>
            </div>

        );
    }
}

const mapStateToProps = (state) => ({
    user:state.authUser.user
})

const mapDispatchToProps = {
    
}


export default  connect(mapStateToProps,mapDispatchToProps)(Profile)