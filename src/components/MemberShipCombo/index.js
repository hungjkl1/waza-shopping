import React, { Component } from 'react'
import Select,{components} from 'react-select'
import {connect} from 'react-redux'
import './style.scss'
import axios from 'axios'
const Option = ({ children, ...props }) => {
    return (
        <components.Option {...props}>
            {props.data.combo_name}
        </components.Option>
    );
  };
const SingleValue = ({ children, ...props }) => {
    return (
        <components.SingleValue {...props}>
          {props.data.combo_name}
        </components.SingleValue>
    );
  };
class MembershipCombos extends Component {
    constructor(props) {
        super(props);
        this.state = ({
            list:[]
        })
    }
    
    componentDidMount(){
        axios
        .post("https://dnguyen-combo-manager.herokuapp.com/comboAPI/Promotions", {
            user_name: this.props.user.coreId,
            service:'shopping'
        })
        .then(result => {
          this.setState({
            list: result.data.combos
          });
        });
    }
    render() {
        return (
            <div>
            <Select
            onChange={value => {
                this.props.onChangeCombo(value)
            }}
            getOptionValue={option => option}
            isDisabled={this.props.isDisable()}
            placeholder='Your Combo'
            classNamePrefix='reward-select' 
            options={this.state.list}
            isSearchable={false}
            isClearable
            components={
                {
                    Option:Option,
                    SingleValue:SingleValue
                }
            }
            />
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    user: state.authUser.user
})

const mapDispatchToProps = {
    
}


export default connect(mapStateToProps, mapDispatchToProps)(MembershipCombos)