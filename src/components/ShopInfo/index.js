import React, { Component } from 'react';
import { Card} from 'react-bootstrap';
import './style.scss';

class ShopInfo extends Component {


    componentDidMount() {

    }
    render() {
        
        return (
            <div className="container-fluid shop-container">
                <div className="row">
                    <div className="col-12">
                        <Card className='container-fluid shop-container-card-header'>
                            <div className="row ">
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6 shop-image">
                                    <img src='image/shop-img.jpg'
                                        className='img-fluid'
                                        alt={this.props.shop.name} />
                                </div>
                                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-6">
                                    <div className="shop-card-header-info">
                                        <h4 className="shop-title">
                                            <strong>
                                                <span>{this.props.shop.name}</span>
                                            </strong>
                                        </h4>
                                        <p className="shop-address">
                                            {this.props.shop.address}
                                        </p>
                                        <p className="shop-phone">
                                            {this.props.shop.phone}
                                        </p>
                                        <ul className="shop-rating">
                                            <i className='fa fa-star'></i>
                                            <i className='fa fa-star'></i>
                                            <i className='fa fa-star'></i>
                                            <i className='fa fa-star'></i>
                                            <i className='fa fa-star-o'></i>
                                        </ul>
                                        <span className="fa fa-circle shop-status">
                                            &nbsp;Open
                                    </span>
                                        &nbsp; &nbsp;
                                    <p className="fa fa-clock-o shop-open-time">
                                        </p>
                                       
                                    </div> 
                                                   
                                </div>
                            </div>
                        </Card>
                    </div>

                </div>

            </div>
        );
    }
}

export default ShopInfo;