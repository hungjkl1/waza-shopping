import { fetchingShop } from "./action";
import API from "../../../core/service";
const service = new API();

export const requestGetShop = params => {
  return dispatch => {
    service
      .post("getActiveShops", params)
      .then(result => {
        return dispatch(fetchingShop(result))
      })
      .catch(err => {});
  };
};
