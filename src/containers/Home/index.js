import React, { Component } from "react";
import { Router, Redirect } from "@reach/router";
import { userLogout } from "../AuthUser/action";
import { connect } from "react-redux";
// Component
import Navbar from "../Navbar";
import MainHome from "./MainHome";
import ShopDetail from "../ShopDetail";
import ShoppingCart from '../ShoppingCart';
import Profile from "../../components/Profile";
import ModalRatingShop from './Rating/Modal';
import { fetchUserLocation } from "../../components/Profile/UserAddress/thunk";
import { liveQuery } from "../../helper/liveQuery";
class Home extends Component {
  
  componentDidMount () {
    this.props.fetchUserAddress()
    liveQuery(this.props.user)
  }
  render() {
    if (!this.props.token||this.props.token==='undefined') {
      console.log("here");
      return <Redirect to="/auth" noThrow />;
    }
    return (
      <div>
        <Navbar logOut={this.props.userLogout} user={this.props.user} />
       
        <Router>
        
          <MainHome path="/" />
          <ShopDetail path="/shop/:id" />
          <ShoppingCart path='cart' />
          <Profile path="account/:id" />
        </Router>
        {/* Modal Rating */}     
        <ModalRatingShop/>

      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  token: state.authUser.token,
  user: state.authUser.user
})

const mapDispatchToProps = dispath => {
  return {
    fetchUserAddress: () => dispath(fetchUserLocation()),
    userLogout: () => dispath(userLogout())
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
