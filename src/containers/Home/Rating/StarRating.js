import StarRatings from 'react-star-ratings';
import React, { Component } from 'react';

class StarRating extends Component {
  constructor(props) {
    super(props);

    this.state = {
      rating: 0
    };

    this.changeRating = this.changeRating.bind(this);
  }

    changeRating(newRating, name) {
      this.setState({
        rating: newRating
      });
    }

  render() {



    return (
      <form style={{ textAlign: "center" }}>
        <StarRatings
          rating={this.state.rating}
          starRatedColor="blue"
          changeRating={this.changeRating}
          numberOfStars={5}
          name='rating'
          starDimension="30px"
          starSpacing="15px"
        />
      </form>
    );
  }
}

export default StarRating;



