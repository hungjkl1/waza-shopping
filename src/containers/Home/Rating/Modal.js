import React, { useEffect } from 'react'
import { Modal, Form } from 'react-bootstrap'
import StarRating from './StarRating';
import ModalRatingDriver from './ModalDriver';
import { closeRatingForm } from './actions';
import {connect} from 'react-redux'

function Detail({
    isOpen,
    bill,
    closeModal
}) {

    useEffect(() => {

    },[bill])
    return (
        <React.Fragment>
            <div >
                <Modal style={{ marginTop: "80px" }} show={isOpen} onHide={closeModal}>
                    <Modal.Header>
                        <Modal.Title className="text-center">Rating Shop</Modal.Title>
                    </Modal.Header>

                    <Modal.Body >
                        <img style={{ height: "100px", width: "100px", display: "block", marginLeft: "auto", marginRight: "auto" }}
                            src='image/shop-img.jpg'
                            className='img-fluid'
                            alt="" />

                        <h4 style={{ textAlign: "center", marginTop: "10px" }} className="shop-title">
                            <strong>
                                <span>Cửa Hàng Bán Rau</span>
                            </strong>
                        </h4>

                        <StarRating />

                        <Form.Control style={{ marginTop: "10px" }} as="textarea" placeholder="Enter Your Comment" rows="3" />
                    
                    </Modal.Body>

                    <Modal.Footer>
                        <ModalRatingDriver handleCloseShop={closeModal} />
                    </Modal.Footer>
                </Modal>
            </div>
        </React.Fragment>
    );
}

const mapStateToProps = (state) => ({
    bill: state.ratingModal.bill,
    isOpen: state.ratingModal.openModal
})

const mapDispatchToProps = dispatch => ({
    closeModal: () => dispatch(closeRatingForm())

})


export default connect(mapStateToProps,mapDispatchToProps)(Detail)