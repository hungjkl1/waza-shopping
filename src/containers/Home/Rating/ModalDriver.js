import React, { useState } from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import StarRating from './StarRating';


export default function Deatail({ handleCloseShop }) {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);



    return (
        <>
            <div>
                <Button variant="primary" onClick={handleShow}>
                    NEXT
                </Button>
            </div>

            <div >
                <Modal style={{ marginTop: "80px" }} show={show} onHide={handleClose}>
                    <Modal.Header closeButton>
                        <Modal.Title className="text-center">Rating Driver</Modal.Title>
                    </Modal.Header>

                    <Modal.Body >
                        <img style={{ height: "100px", width: "100px", display: "block", marginLeft: "auto", marginRight: "auto" }}
                            src='https://www.colibrilife.com/wp-content/uploads/2018/01/no-avatar-female.png'
                            className='img-flui'
                            alt="" />

                        <h4 style={{ textAlign: "center", marginTop: "10px" }} className="shop-title">
                            <strong>
                                <span>Ten Tai Xe</span>
                            </strong>
                        </h4>


                        <StarRating />

                        <Form.Control style={{ marginTop: "10px" }} as="textarea" placeholder="Enter Your Comment" rows="3" />
                    </Modal.Body>

                    <Modal.Footer>
                        <Button style={{ backgroundColor: "green", color: "black" }} variant="primary" onClick={() => {
                            handleClose()
                            handleCloseShop()
                            }}>
                            SEND
                            </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        </>
    );
}


