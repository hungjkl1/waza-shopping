export const OPEN_RATING_FORM = 'OPEN_RATING_FORM'
export const CLOSE_RATING_FORM = 'CLOSE_RATING_FORM'


export const openRatingForm = (payload) => ({
    type: OPEN_RATING_FORM,
    payload
})

export const closeRatingForm = () => ({
    type: CLOSE_RATING_FORM,
})
