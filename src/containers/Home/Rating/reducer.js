import { OPEN_RATING_FORM, CLOSE_RATING_FORM } from "./actions"

export const initialState = {
    bills: [],
    bill: {},
    openModal: false
}

const Rating = (state = initialState, { type, payload }) => {
    switch (type) {

    case OPEN_RATING_FORM:
        return { ...state, openModal:true, bill:payload }

    case CLOSE_RATING_FORM:
        return { ...state, openModal:false }
    default:
        return state
    }
}
export default Rating