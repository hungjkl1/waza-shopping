import React, { Component } from 'react';
import {Modal} from 'react-bootstrap';
import AddressForm from './form'
import './style.scss';
class LocationModal extends Component {
    render() {
        return (
          <Modal className='new-address' show={this.props.show} onHide={this.props.handleOpenClose} >
            <Modal.Header closeButton>
              <Modal.Title>Add your new address</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <AddressForm onSubmit={this.props.handleAddAddress}/>
            </Modal.Body>
          </Modal>
        );
    }
}


LocationModal.propTypes = {
    
};


export default LocationModal;
