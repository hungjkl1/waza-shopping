import React, { Component } from 'react'
import LocationSearch from '../../components/LocationSearch'
import { Fields , reduxForm } from 'redux-form'

class AddressForm extends Component {
    render() {
        return (
            <form onSubmit={this.props.handleSubmit}>
                <Fields names={['address','lng','lat']} component={LocationSearch}/>
                <button type='submit' className='btn btn-info btn-block'>Add</button>
          </form>
        )
    }
}

export default reduxForm({
    form: 'address',
})(AddressForm)