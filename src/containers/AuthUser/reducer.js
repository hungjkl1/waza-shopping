import {SIGN_UP_REQUEST, GET_CURRENT_USER,LOGIN_REQUEST,LOG_OUT, ADD_WALLET, GET_WALLET_INFO} from './action'

const initialState = {
    token:null,
    user:{},
    balance:{}
}

const passengerAuth = (state = initialState, action) => {

    switch (action.type) {
    case SIGN_UP_REQUEST: {
        localStorage.setItem('token',action.data.sessionToken)
        localStorage.setItem('user',JSON.stringify(action.data))
        const signup = {
            token: action.data.sessionToken,
            user: action.data
        }
        return signup
    }
    case LOGIN_REQUEST: {
        localStorage.setItem('token',action.data.sessionToken)
        localStorage.setItem('user',JSON.stringify(action.data))
        const login = {
            token: action.data.sessionToken,
            user: action.data
        }
        return login
    }
    case GET_CURRENT_USER: {
        const currentUser = {
            token: action.data.token,
            user: action.data.user
        }
        return currentUser
    }
    case GET_WALLET_INFO: {
        const walletInfo = {
            ...state,
            balance: action.data
        }
        return walletInfo
    }
    case ADD_WALLET: {
        const newInfo = {
            token: state.token,
            user: action.data
        }
        localStorage.setItem('user',JSON.stringify(newInfo.user))
        return newInfo
    }
    case LOG_OUT: {
        localStorage.removeItem('token')
        localStorage.removeItem('user')
        return initialState
    }
    default:
        return state
    }
}

export default passengerAuth