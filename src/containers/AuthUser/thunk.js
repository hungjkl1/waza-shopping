import { requestSignUpPassenger, userLogin,addWallet } from "./action";
import API from "../../core/service";
import Swal from "sweetalert2";

const service = new API();

export const SignUp = params => {
  return dispatch => {
    service
      .post("userSignin", params)
      .then(result => {
        console.log(result);
        if (result.data.code === 203 || result.data.code === 202) {
          return Swal.fire({
            text: result.data.message,
            type: "error",
            confirmButtonText: "Please Sign Up Again"
          });
        } else if (result.data.phone==="Bad phone format") {
          return Swal.fire({
            text: 'Please use vn phone number',
            type: "error",
            confirmButtonText: "Please Login Again"
          })
        } else if (result.data.error) {
          return Swal.fire({
            text: result.data.error,
            type: "error",
            confirmButtonText: "Please Login Again"
          })
        } else if (result.data === 'Account already existed in waza system please login with your waza account') {
          return Swal.fire({
            text: result.data,
            type: "error",
            confirmButtonText: "Please Login Again"
          })
        }
         else {
          Swal.fire({
            title: "Signup success",
            html: "You will be redirect to homepage real soon",
            timer: 2000
          }).then(() => {
            dispatch(requestSignUpPassenger(result.data));
          });
        }
      })
      .catch(err => {
        console.log(err);
      });
  };
};

export const LoginUser = params => {
  return dispatch => {
    service
      .post("userLogin", params)
      .then(result => {
        console.log(result.data.error);
        if (result.data.code === 203) {
          return Swal.fire({
            text: result.data.message,
            type: "error",
            confirmButtonText: "Please Login Again"
          });
        } else if (result.data.error) {
          return Swal.fire({
            text: result.data.error,
            type: "error",
            confirmButtonText: "Please Login Again"
          });
        }  else {
          Swal.fire({
            title: "Signup success",
            html: "You will be redirect to homepage real soon",
            timer: 2000
          }).then(() => {
            dispatch(userLogin(result.data));
          });
        }
      })
      .catch(error => {
        return Swal.fire({
          text: error.error,
          type: "error",
          confirmButtonText: "Please Login Again"
        });
      });
  };
};

export const addWalletThunk = (params) => {
  return dispatch => {
    service.post('addWallet',params)
    .then((result) => {
      if(result.data === 'Please login to charge your wallet'|| result.data === 'Please charge more than 50.000VND')
      return Swal.fire({
        text: result.data,
        type: "error",
        confirmButtonText: 'Close'
      })
      dispatch(addWallet(result.data))
      return Swal.fire({
        text: `You just add ${params.money}VND to your account wallet`,
        type: "success",
        confirmButtonText: 'Close'
      })
    }).catch((err) => {
      return Swal.fire({
        text: err.error,
        type: "error",
        confirmButtonText: 'Close'
      })
    });
  }
}