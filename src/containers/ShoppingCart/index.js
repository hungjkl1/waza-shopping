import React from "react";
import Item from "./Item";
import "./ShoppingCart.scss";
// Redux
import { connect } from "react-redux";
import LocationSelect from "../../components/LocationSelect";
import _ from "lodash";
import axios from "axios";
import MembershipCombo from "../../components/MemberShipCombo";
import MemberShipVoucher from "../../components/MemberShipVoucher";
import VoucherReward from "../../components/VoucherReward";
import Select from "react-select";
import { getDiscountMember, checkOut } from "./thunk";
import Swal from "sweetalert2";
import { getMoney } from "../../components/Profile/CustomerWallet/thunk";
const PayWith = [
  { value: "WALLET", label: "Pay With WAZA Wallet" },
  { value: "CASH", label: "Pay With Cash" }
];

class ShoppingCart extends React.Component {
  constructor(props) {
    super(props);
    this.cash = 0
    this.wallet = 0
    this.state = {
      location: {},
      shippingDistance: null,
      shippingFee: 0,
      promotion_id: null,
      promotion_type: null,
      discount: null,
      service: "shopping",
      voucherFrom: null,
      reward: null,
      subtotal:0,
      payWith: "WALLET",
      start_lat: null,
      start_lon: null,
      end_lat: null,
      end_lon: null,
      start_address: null,
      end_address: null,
      passengerId: this.props.user.coreId,
    };
  }

  onChangeLocation = async location => {
    const { shop = {} } = this.props.cartItems;
    try {
      const distance = await axios
        .post("https://waza-trip.herokuapp.com/show_trip", {
          service_key: 1,
          start_lat: shop.ltn,
          start_lon: shop.lng,
          end_lat: location.lat,
          end_lon: location.lng,
          start_address: shop.address,
          end_address: location.address
        })

        const fee = await axios.post("https://waza-payment.herokuapp.com/get_trip_price", {
          distance:distance.data.response.distance / 1000,
          service: 'SHOPPING'
        })

        this.setState({
          location: location,
          start_lat: shop.ltn,
          start_lon: shop.lng,
          end_lat: location.lat,
          end_lon: location.lng,
          start_address: shop.address,
          end_address: location.address,
          shippingDistance: distance.data.response.distance,
          shippingFee: Math.round(fee.data.response)
        });
    }
      catch(err){
        this.setState({
          location: location,
          shippingDistance: 0,
          shippingFee: 0,
          start_lat: null,
          start_lon: null,
          end_lat: null,
          end_lon: null,
          start_address: null,
          end_address: null
        });
  }
}

  onChangePayment = payment => {
    this.setState({
      payWith: payment.value
    });
  };

  onChangeCombo = async (combo = {}) => {
    if(combo) {
      try{
        const discount = await getDiscountMember({
          user_name: this.props.user.coreId,
          total: this.getTotal() === 0 ? 1:this.getTotal(),
          service: this.state.service,
          promotion_id: combo.combo_id,
          promotion_type: "combo"
        });
        this.setState({
          promotion_id: combo.combo_id,
          promotion_type: "combo" ,
          voucherFrom: "membershipCombo" ,
          subtotal: discount.data.reduceMoney,
          reward: null
        });
      }
      catch (err) {
        this.setState({
          promotion_id: null,
          promotion_type: null,
          voucherFrom: null,
          subtotal: 0,
          reward: null
        });
      }
    } else {
      this.setState({
        promotion_id: null,
        promotion_type: null,
        voucherFrom: null,
        subtotal: 0,
        reward: null
      });
    }
  };

  onChangeVoucher = async (voucher = {}) => {
    if (voucher) {
      try {
        const discount = await getDiscountMember({
          user_name: this.props.user.coreId,
          total: this.getTotal() === 0 ? 1:this.getTotal(),
          service: this.state.service,
          promotion_id: voucher.voucher_id,
          promotion_type: "voucher"
        });
        this.setState({
          promotion_id: voucher.voucher_id ,
          promotion_type: "voucher" ,
          voucherFrom: "membershipVoucher" ,
          subtotal: discount.data.reduceMoney,
          reward: null
        });
      } catch(err) {
        this.setState({
          promotion_id: null,
          promotion_type: null,
          voucherFrom: null,
          subtotal:0,
          reward: null
        });
      }
    } else {
      this.setState({
        promotion_id: null,
        promotion_type: null,
        voucherFrom: null,
        subtotal:0,
        reward: null
      });
    }
  };

  onChangeReward = async (reward = {}) => {
    if(reward) {
      try {
        this.setState({
          voucherFrom: 'rewardVoucher',
          subtotal: reward.ValueDiscount,
          reward: reward.value,
        });
      } catch (error) {
        this.setState({
          voucherFrom: null,
          subtotal: 0,
          reward: null
        });
      }
    } else {
      this.setState({
        voucherFrom: null,
        subtotal:0,
        reward: null
      });
    }
  };

  checkOut = () => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, i want to checkout!"
    }).then(value => {
      if(value) {
        checkOut({...this.state,billDetail:this.props.cartItems.items,total:this.getTotal()})
      }
    })
  }
  disableCheckout = () => {
    const { shippingDistance, payWith } = this.state;
    if (shippingDistance === null || !payWith || _.isEmpty(this.props.cartItems.items)) return true;
    return false;
  };

  getTotal = () => {
    let total = 0;
    this.props.cartItems.items.map(item => {
      return (total += item.quantity * item.price);
    });
    if (this.state.shippingFee) {
      total += this.state.shippingFee;
    }
    if (this.state.subtotal > 1) {
      total = total - this.state.subtotal;
    }
    if (this.state.subtotal <= 1 && this.state.subtotal > 0) {
      total = total * this.state.subtotal
    }
    if(total<0){
      return 0
    }
    return total;
  };
  componentDidMount(){
    this.props.getWalletInfo(this.props.user.coreId)
  }
  render() {
    const { shop = {} } = this.props.cartItems;
    return (
      <div className="shoppingcart">
        <p className="title">SHOPPING CART</p>
        <div className="row">
          <div className="col-12">
            <h6>Shop: {shop.name}</h6>
          </div>
          <div className="col-12">
            <h6>Shop address: {shop.address}</h6>
          </div>
        </div>
        <div className="table-responsive-md">
          <table className="table text-left">
            <thead>
              <tr>
                <th></th>
                <th>Product</th>
                <th>Price</th>
                <th>Quantity</th>
                <th>Total</th>
                <th></th>
              </tr>
            </thead>
            {/* Danh sách sản phẩm trong giỏ hàng */}
            {this.props.cartItems.items.map((product, index) => {
              return <Item key={index} product={product} />;
            })}
          </table>
        </div>
        {/* Phần tính tổng - thanh toán */}
        <hr />
        <div className="total ">
          <div className="row service">
            <div className="col-12 col-md-6 col-lg-3">
              <div>Your address</div>
              <LocationSelect onChangeLocation={this.onChangeLocation} />
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div>Membership Combo</div>
              <MembershipCombo
                isDisable={() => {
                  if (
                    this.state.voucherFrom === "membershipCombo" ||
                    this.state.voucherFrom === null
                  ) {
                    return false;
                  }
                  return true;
                }}
                onChangeCombo={this.onChangeCombo}
              />
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div>Membership Voucher</div>
              <MemberShipVoucher
                isDisable={() => {
                  if (
                    this.state.voucherFrom === "membershipVoucher" ||
                    this.state.voucherFrom === null
                  ) {
                    return false;
                  }
                  return true;
                }}
                onChangeCombo={this.onChangeVoucher}
              />
            </div>
            <div className="col-12 col-md-6 col-lg-3">
              <div>Reward Voucher</div>
              <VoucherReward
                isDisable={() => {
                  if (
                    this.state.voucherFrom === "rewardVoucher" ||
                    this.state.voucherFrom === null
                  ) {
                    return false;
                  }
                  return true;
                }}
                onChangeCombo={this.onChangeReward}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-12 col-lg-4">
              <textarea placeholder="Description....."></textarea>
            </div>
            <div className="col-12 col-lg-8">
              <div className="row">
                <div className="col-6">
                  DISTANCE :{" "}
                  <span>
                    ~{_.round(this.state.shippingDistance / 1000, 2)}KM
                  </span>
                </div>
                <div className="col-6">
                  SHIPPING FEE : <span>{this.state.shippingFee}VND</span>
                </div>
                <div className="col-6">
                  SUBTOTAL : <span>{this.state.subtotal}</span>
                </div>
                <div className="col-6">
                  TOTAL : <span>{this.getTotal()} VND</span>
                </div>
              </div>
              <div className="payment-method">
                {this.state.payWith === "WALLET" &&
                  `Your waza wallet: ${this.props.balance.wallet}VND`}
                  {this.state.payWith === "CASH" &&
                  `Your waza cash: ${this.props.balance.cash}VND`}
                <Select
                  onChange={this.onChangePayment}
                  defaultValue={PayWith[0]}
                  options={PayWith}
                />
              </div>
              <button
                onClick={this.checkOut}
                disabled={this.disableCheckout()}
                className="btn btn-block checkout"
              >
                Checkout
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    user: state.authUser.user,
    cartItems: state.cartItems,
    balance: state.authUser.balance ? state.authUser.balance : {}
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getWalletInfo: id => dispatch(getMoney(id))
  }
}
export default connect(mapStateToProps,mapDispatchToProps)(ShoppingCart);
