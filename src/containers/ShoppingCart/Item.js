import React, { Component } from 'react';
// Redux
import { connect } from 'react-redux';
import { removeProduct, changeQuantityProduct } from './action';
class Item extends Component {

  // constructor(props) {
  //   super(props);
  // }
  handleChangeQuantity = (item, quantity) => {
    this.props.dispatch(changeQuantityProduct(item, quantity))
  };
  handleRemoveItem = (item) => {
    this.props.dispatch(removeProduct(item))
  };
  getTotalPrice = () => {
    const { price, quantity } = this.props.product
    return price * quantity;
  };
  render() {
    const { quantity, name, price } = this.props.product;
    return (
      <tbody>
        <tr >
          <td><img className="img img-fluid" alt='product-img'
            src='https://cdn3.iconfinder.com/data/icons/food-155/100/Healthy_food_1-512.png' /></td>
          <td>{name}</td>
          <td>{price}</td>
          <td>
            <p className="quantity">
              <button disabled={quantity <= 1}
                onClick={() => this.handleChangeQuantity(this.props.product, quantity - 1)}> - </button>
              <span>{quantity}</span>
              <button disabled={quantity >= 99}
                onClick={() => this.handleChangeQuantity(this.props.product, quantity + 1)}> + </button>
            </p>
          </td>
          <td>{this.getTotalPrice()}</td>
          <td><button className="btn btn-danger glyphicon glyphicon-remove"
            onClick={() => this.handleRemoveItem(this.props.product)}> X </button></td>
        </tr>
      </tbody>
    );
  };
};

export default connect(null, null)(Item);
