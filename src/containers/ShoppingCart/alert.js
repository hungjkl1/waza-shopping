import Swal from 'sweetalert2'
export const addSuccess = Swal.mixin({
  toast: true,
  position: 'center',
  showConfirmButton: false,
  timer: 1500,
  type: 'success',
  title: 'Added successfully'
});

export const addError = Swal.mixin({
  toast: true,
  position: 'center',
  showConfirmButton: false,
  timer: 1500,
  type: 'error',
  title: 'Product quantity over 99'
});


export const addOtherShop = Swal.mixin({
  toast: true,
  type: 'info',
  title: 'Added from other shop',
  position: 'center',
  showConfirmButton: false,
  timer: 1500
});