//import { compareInCart } from '../../helper/cartCompare';
import { ADD_TO_CART, REMOVE_PRODUCT, CHANGE_PRODUCT_QUANTITY } from './action';
import * as alert from './alert';

const cartItems = JSON.parse(localStorage.getItem('cartItems'))
const initialState = cartItems ? cartItems : { shop: {}, items: [] };

const shoppingCart = (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_CART:
      const cartItems = JSON.parse(localStorage.getItem('cartItems'))
      // Kiểm tra xem giỏ hàng có tồn tại
      if (cartItems) {
        // Sản phẩm được thêm có cùng của tiệm
        if (cartItems.shop.objectId === action.item.shop.objectId) {
          // Cùng cửa hàng - Kiểm tra trùng sản phẩm
          const existItem = cartItems.items.findIndex(({ objectId }) => {
            return objectId === action.item.objectId
          });
          if (existItem !== -1) {
            // Sản phẩm trùng
            const checkingVarible = cartItems.items[existItem].quantity + Number(action.item.quantity);
            if (checkingVarible < 1 || checkingVarible > 99) {
              // Sản phẩm trên 99 hoặc bé hơn 1
              alert.addError.fire();
              cartItems.items[existItem].quantity = 99;
              localStorage.setItem('cartItems', JSON.stringify(cartItems));
              return { ...state, ...cartItems }
            } else {
              alert.addSuccess.fire();
              cartItems.items[existItem].quantity += Number(action.item.quantity);
              localStorage.setItem('cartItems', JSON.stringify(cartItems));
              return { ...state, ...cartItems }
            }

          } else {
            // Sản phẩm không trùng
            alert.addSuccess.fire();
            cartItems.items = [...cartItems.items, action.item];
            localStorage.setItem('cartItems', JSON.stringify(cartItems));
            return { ...state, ...cartItems }
          };
        } else {
          // Khác cửa hàng 
          alert.addOtherShop.fire()
          const newCartItems = {
            shop: action.item.shop,
            items: [action.item]
          };
          localStorage.setItem('cartItems', JSON.stringify(newCartItems));
          return { ...state, ...newCartItems }
        }
      } else {
        // Khởi tạo giỏ hàng
        alert.addSuccess.fire()
        const firstCartItems = {
          shop: action.item.shop,
          items: [action.item]
        };
        localStorage.setItem('cartItems', JSON.stringify(firstCartItems));
        return { ...state, ...firstCartItems }
      }
    case REMOVE_PRODUCT:
      const cartItemsA = JSON.parse(localStorage.getItem('cartItems'))
      cartItemsA.items = cartItemsA.items.filter((item) => {
        return !(item.objectId === action.item.objectId);
      });
      localStorage.setItem('cartItems', JSON.stringify(cartItemsA));
      return { ...state, ...cartItemsA }
    case CHANGE_PRODUCT_QUANTITY:
      const cartItemsB = JSON.parse(localStorage.getItem('cartItems'));
      const existItem = cartItemsB.items.findIndex(({ objectId }) => {
        return objectId === action.item.objectId
      });
      cartItemsB.items[existItem].quantity = Number(action.quantity);
      localStorage.setItem('cartItems', JSON.stringify(cartItemsB));
      return { ...state, ...cartItemsB }
    default:
      return state
  }
}

export default shoppingCart