import axios from "axios";
import API from "../../core/service";
import Swal from "sweetalert2";
import _ from 'lodash'
const service = new API();

export const getDiscountMember = params => {
  return axios
    .post(
      "https://dnguyen-combo-manager.herokuapp.com/comboAPI/PromotionCheck",
      params
    )
    .then(result => {
      return result;
    })
    .catch(err => {
      console(err);
    });
};

export const getDiscountReward = params => {
  return axios
    .post(
      "https://safe-caverns-44957.herokuapp.com/API/Reward/GetRewardByCode",
      params
    )
    .then(result => {
      return result.data.rewardInfo;
    })
    .catch(err => {
      return err;
    });
};

export const checkOut = params => {
  return service
    .post("createBill", params)
    .then(result => {
      return axios.post('https://waza-trip.herokuapp.com/pass_accepted',{
          service_key: 4,
          service_trip_id:`SHOPPING4${result.data[0].bill.objectId}`,
          passenger_id: params.passengerId,
          passenger_response: 'Accepted',
          start_lat: params.start_lat,
          start_lon: params.start_lon,
          end_lat:params.end_lat,
          end_lon:params.end_lon,
          start_address: params.start_address,
          end_address: params.end_address,
          distance: params.shippingDistance,
          price:params.total,
          original_price: params.total+params.subtotal,
          promotion_id: params.promotion_id ? params.promotion_id : params.reward ? params.reward : '',
          promotion_type: params.promotion_id ? 'Combo' : params.reward ? 'Reward' : '',
          payment_type: params.payWith,
        }).then((result) => {
          if(!result.data.status) {
            return Swal.fire({
              title: 'We cant find you any drivers near your location',
              icon: 'error  '
            })  
          }
          return Swal.fire({
            title: 'You will get noti when we find you a driver',
            icon: 'success'
          })
        }).catch((err) => {
          Swal.fire({
            title: 'Cant find any driver',
            icon: "error"
          })
        });
    })
    .catch(err => {});
};
