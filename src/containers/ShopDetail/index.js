import React, { Component } from "react";
import { getShopDetail } from "./thunk";
import { connect } from "react-redux";
import ShopInfo from "../../components/ShopInfo";
// import ShopMenu from '../../components/ShopMenu';
import ShopItem from "../../components/ShopItem";
import { addProduct } from "../ShoppingCart/action";
import "./style.scss";

class ShopDetail extends Component {
  componentDidMount() {
    this.props.getShopDetail({ shopId: this.props.id });
  }

  addProductToCart = item => {
    this.props.addProduct({ cart: item, shop: this.props.shop });
  };

  render() {
    return (
      <div>
        {/* Shop info */}
        <ShopInfo shop={this.props.shop} />
        {/* Shop Menu */}
        <div className="container">
          <hr />
          <div className="row">
            <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <ShopItem addProduct={this.addProductToCart} product={this.props.product} />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  shop: state.shopDetail.shopDetail,
  product: state.shopDetail.productDetails
});

const mapDispatchToProps = dispatch => {
  return {
    addProduct: item => dispatch(addProduct(item)),
    getShopDetail: params => dispatch(getShopDetail(params))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShopDetail);
