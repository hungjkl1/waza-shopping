export const GET_SHOP_DETAIL = 'GET_SHOP_DETAIL'

export const requestGetShopDetail = (payload) => ({
    type: GET_SHOP_DETAIL,
    payload
})
