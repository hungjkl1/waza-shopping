import {requestGetShopDetail} from './action'
import API from '../../core/service'
const service = new API()
export const getShopDetail = (params) => {
    return dispatch => {
        service.post('getShopDetail',params).then((result) => {
            dispatch(requestGetShopDetail(result))
        }).catch((err) => {
            console.log(err)
        });
    }
}