import {GET_SHOP_DETAIL} from './action';
const initialState = {
    shopDetail:{},
    productDetails:[]
}
const shopDetail = (state = initialState, { type, payload }) => {
    switch (type) {
    case GET_SHOP_DETAIL:
        return { ...state, ...payload.data }

    default:
        return state
    }
}
export default shopDetail